package models;

/**
 * Created by minhcao on 4/29/16.
 */
public class Group {
    private Long id;
    private String name;
    private int privacy;
    private int size;

    private int hasJoined;

    // Database info
    public static final String TABLE_NAME = "groups";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_PRIVACY = "privacy";
    public static final String COLUMN_SIZE = "size";
    public static final String COLUMN_HASJOINED = "hasJoined";
    public static final String TABLE_CREATE = "create table if not exists "
                            + TABLE_NAME + "("
                            + COLUMN_ID + " integer primary key autoincrement, "
                            + COLUMN_HASJOINED + " integer default 0,"
                            + COLUMN_SIZE + " integer, "
                            + COLUMN_NAME + " text not null unique ON CONFLICT REPLACE, "
                            + COLUMN_PRIVACY + " int not null);";

    public Group(Long id, String name, int privacy, int size, int hasJoined) {
        this.id = id;
        this.name = name;
        this.privacy = privacy;
        this.size = size;
        this.hasJoined = hasJoined;
    }

    public Group() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrivacy() {
        return privacy;
    }

    public void setPrivate(int aPrivate) {
        privacy = aPrivate;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getHasJoined() {
        return hasJoined;
    }

    public void setHasJoined(int hasJoined) {
        this.hasJoined = hasJoined;
    }
}
