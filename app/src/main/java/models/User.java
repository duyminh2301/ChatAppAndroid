package models;

/**
 * Created by minhcao on 4/29/16.
 */
public class User {
    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String title;
    private String age;
    private String role;
    private String avatar;

    // Database info
    public static final String TABLE_NAME = "users";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_FIRSTNAME = "firstname";
    public static final String COLUMN_LASTNAME = "lastname";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_ROLE = "role";
    public static final String COLUMN_AGE = "age";
    public static final String COLUMN_AVATAR = "avatar";
    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key autoincrement, "
            + COLUMN_USERNAME + " text not null unique, "
            + COLUMN_FIRSTNAME + " text, "
            + COLUMN_LASTNAME + " text, "
            + COLUMN_EMAIL + " text, "
            + COLUMN_TITLE + " text, "
            + COLUMN_ROLE + " text not null, "
            + COLUMN_AGE + " text, "
            + COLUMN_AVATAR + " text);";

    public User(Long id, String username, String firstName, String lastName, String email, String title, String age, String role, String avatar) {
        this.id = id;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.title = title;
        this.age = age;
        this.role = role;
        this.avatar = avatar;
    }

    public User() {

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
