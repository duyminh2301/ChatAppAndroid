package models;

import java.util.Date;
import java.util.Set;

/**
 * Created by minhcao on 4/29/16.
 */
public class GroupEntry extends Entry{
    // Database info
    public static final String TABLE_NAME = "groupentry";

    public static final String TABLE_CREATE = "create table if not exists "
            + TABLE_NAME + "("
            + COLUMN_ID + " integer primary key, "
            + COLUMN_TIME + " text not null, "
            + COLUMN_ORIGIN + " text not null, "
            + COLUMN_TARGET + " text not null, "
            + COLUMN_MESSAGE + " text, "
            + COLUMN_FILEPATH + " text, "
            + COLUMN_FILETYPE + " text);";
}
