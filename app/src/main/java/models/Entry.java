package models;

import java.util.Set;

/**
 * Created by minhcao on 5/3/16.
 */
public class Entry {

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TIME = "time";
    public static final String COLUMN_ORIGIN = "origin";
    public static final String COLUMN_TARGET = "target";
    public static final String COLUMN_MESSAGE = "message";
    public static final String COLUMN_FILEPATH = "filepath";
    public static final String COLUMN_FILETYPE = "filetype";

    private Long id;
    private String time;
    private String origin;
    private String target;
    private String message;
    private String filePath;
    private String fileType;
    private Set<User> readUser;

    public Entry(){
    }

    public Entry(Long id, String time, String origin, String target, String message, String filePath, String fileType, Set<User> readUser) {
        this.id = id;
        this.time = time;
        this.origin = origin;
        this.target = target;
        this.message = message;
        this.filePath = filePath;
        this.fileType = fileType;
        this.readUser = readUser;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public Set<User> getReadUser() {
        return readUser;
    }

    public void setReadUser(Set<User> readUser) {
        this.readUser = readUser;
    }
}