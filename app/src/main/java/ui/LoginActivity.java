package ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.minhcao.chatapp.R;

import java.io.IOException;

import helpers.ChatApplication;
import helpers.Endpoints;
import helpers.RESTClient;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by AnhDuyPham on 4/30/2016.
 */
public class LoginActivity extends Activity {

    private EditText email_input;
    private EditText pwd_input;
    private RESTClient restClient;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        email_input = (EditText) findViewById(R.id.login_email);
        pwd_input = (EditText) findViewById(R.id.login_pwd);

        restClient = ChatApplication.getInstance().getRestClient();

        TextView link_register = (TextView) findViewById(R.id.link_reg);
        link_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent regIntent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(regIntent);
            }
        });

        Button login_btn = (Button) findViewById(R.id.btn_login);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    private void login() {
        // Get inputs
        final String email = email_input.getText().toString();
        String pwd = pwd_input.getText().toString();

        final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this,
                R.style.AppTheme);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();

        // Validate inputs
        if (!(email.equals("") && pwd.equals(""))) {
            // Build and send request
            restClient.auth(Endpoints.LOGIN, email, pwd, new Callback() {
                // Adapt with response
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, final Response response) throws IOException {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (response.isSuccessful()) {
                                //Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                                //startService(homeIntent);
                                // save token
                                try {
                                    String token = response.body().string();
                                    ChatApplication.getInstance().getPrefManager().setToken(token);
                                    ChatApplication.getInstance().getPrefManager().setUsername(email);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                Intent homeIntent = new Intent(LoginActivity.this, HomeActivity.class);
                                startActivity(homeIntent);
                            } else {
                                // wrong password
                                progressDialog.dismiss();
                                email_input.setError("Username is not matched.");
                                pwd_input.setError("Password is not matched.");
                            }
                        }
                    });


                }

            });
        } else {
            // Notify empty
            email_input.setError("Please fill in all fields.");
            pwd_input.setError("Please fill in all fields.");
        }
    }
}