package ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.minhcao.chatapp.R;

import java.io.IOException;

import helpers.ChatApplication;
import helpers.Endpoints;
import helpers.RESTClient;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by AnhDuyPham on 4/30/2016.
 */
public class RegisterActivity extends Activity {

    private EditText email_input;
    private EditText pwd_input;
    private RESTClient restClient;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        email_input = (EditText) findViewById(R.id.reg_email);
        pwd_input = (EditText) findViewById(R.id.reg_pwd);

        restClient = ChatApplication.getInstance().getRestClient();

        TextView link_login = (TextView) findViewById(R.id.link_login);
        link_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(loginIntent);
            }
        });

        Button login_btn = (Button) findViewById(R.id.btn_reg);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do register
                String email = email_input.getText().toString();
                String pwd = pwd_input.getText().toString();

                final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this,
                        R.style.AppTheme);
                progressDialog.setIndeterminate(true);
                progressDialog.setMessage("Registering...");
                progressDialog.show();

                // Validate inputs
                if (!(email.equals("") && pwd.equals(""))) {
                    // Build and send request
                    restClient.auth(Endpoints.REGISTER, email, pwd, new Callback() {

                        // Adapt with response
                        @Override
                        public void onFailure(Call call, IOException e) {
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(Call call, final Response response) throws IOException {

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (response.isSuccessful()) {
                                        // save token
                                        String token = null;
                                        try {
                                            token = response.body().string();
                                            ChatApplication.getInstance().getPrefManager().setToken(token);
                                            ChatApplication.getInstance().getPrefManager().setUsername(email_input.getText().toString());
                                        } catch (IOException e) {
                                            e.printStackTrace();
                                        }
                                        Intent homeIntent = new Intent(RegisterActivity.this, HomeActivity.class);
                                        startActivity(homeIntent);

                                    } else {
                                        progressDialog.dismiss();
                                        email_input.setError("Username are being used. Try another one.");
                                        pwd_input.setError("");

                                    }
                                }
                            });
                        }

                    });

                } else {
                    email_input.setError("Please fill in all fields.");
                    pwd_input.setError("");
                }
            }
        });

    }
}
