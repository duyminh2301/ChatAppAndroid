package ui;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.minhcao.chatapp.R;

import java.io.IOException;

import adapters.MessageListAdapter;
import db.DataProvider;
import helpers.ChatApplication;
import helpers.Endpoints;
import helpers.NotificationUtils;
import helpers.RESTClient;
import helpers.ResponseParser;
import models.Entry;
import models.User;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;
import service.PollingService;

/**
 * Created by minhcao on 4/28/16.
 */
public class ChatBoxActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor> {
    private ListView mMessageListView;
    private MessageListAdapter mMessageListAdapter;
    private String mTargetChat;
    private boolean mIsPrivateChat;
    private Button mSendBtn;
    private RESTClient mClient;
    private EditText mInputText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chatbox);
        Intent receivedIntent = getIntent();
        mTargetChat = receivedIntent.getStringExtra("target");
        mIsPrivateChat = receivedIntent.getBooleanExtra("isPrivate", true);

        mSendBtn = (Button) findViewById(R.id.btnSend);
        mInputText = (EditText) findViewById(R.id.inputMsg);

        mMessageListView = (ListView) findViewById(R.id.messageListView);
        mMessageListAdapter = new MessageListAdapter(ChatBoxActivity.this, null, 0);
        Toolbar toolbar = (Toolbar) findViewById(R.id.chatToolbar);
        toolbar.setTitle(mTargetChat);
        toolbar.setTitleTextColor(Color.WHITE);

        mMessageListView.setAdapter(mMessageListAdapter);

        mClient = ChatApplication.getInstance().getRestClient();

        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_action_hardware_keyboard_backspace);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSendBtn.setOnClickListener(new SendMessageListener());
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        NotificationUtils.clearNotifications();
        // REST call
        String url;
        if (mIsPrivateChat) {
            url = Endpoints.ENTRY_GET + "@" + mTargetChat;
        } else {
            url = Endpoints.ENTRY_GET + mTargetChat;
        }
        mClient.get(url, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    response.body().close();
                    throw new IOException("Unexpected code " + response);
                }
//                Log.d("REST", "is "+ response.body().string());
                ResponseParser.parse(Endpoints.ENTRY_GET, response.body().byteStream());
            }
        });
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        CursorLoader cursorLoader;
        String[] projection = {Entry.COLUMN_ID, Entry.COLUMN_MESSAGE, Entry.COLUMN_ORIGIN, Entry.COLUMN_TIME, Entry.COLUMN_TARGET};
        String user_username = ChatApplication.getInstance().getPrefManager().getUsername();
        if (mIsPrivateChat) {
            String selection = Entry.COLUMN_TARGET + " in (?,?) AND "+Entry.COLUMN_ORIGIN+" in (?,?)";
//            String selection = Entry.COLUMN_TARGET +" = ? AND "+Entry.COLUMN_ORIGIN+" = ? OR "+
//            Entry.COLUMN_TARGET +" = ? AND "+Entry.COLUMN_ORIGIN+" = ?";
            String[] selectionArgs = {mTargetChat, user_username, user_username, mTargetChat};
            cursorLoader = new CursorLoader(ChatBoxActivity.this, DataProvider.PRIVATE_ENTRY_URI, projection, selection, selectionArgs, null);
        } else {
            String selection = Entry.COLUMN_TARGET + " = ?";
            String[] selectionArgs = {mTargetChat};
            cursorLoader = new CursorLoader(ChatBoxActivity.this, DataProvider.GROUP_ENRTY_URI, projection, selection, selectionArgs, null);
        }
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mMessageListAdapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mMessageListAdapter.swapCursor(null);
    }

    private class SendMessageListener implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            String message = mInputText.getText().toString();
            String url;
            if (mIsPrivateChat) {
                url = Endpoints.MESSAGE_SEND + "@" + mTargetChat;
            } else {
                url = Endpoints.MESSAGE_SEND + mTargetChat;
            }
            mClient.post(url, composeXML(message), new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()){
                        response.body().close();
                        throw new IOException("Unexpected code " + response);
                    }
                    response.body().close();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mInputText.setText("");
                        }
                    });
                }
            });
        }

        private String composeXML(String message) {
            String origin = ChatApplication.getInstance().getPrefManager().getUsername();
            StringBuffer str = new StringBuffer();
            str.append("<historyEntry><origin><username>");
            str.append(origin);
            str.append("</username></origin><message>");
            str.append(message);
            str.append("</message><time>null</time></historyEntry>");
            return str.toString();
        }
    }
}

