package ui;

import android.app.Dialog;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;

import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;

import com.example.minhcao.chatapp.R;

import java.io.IOException;

import adapters.PublicGroupMgmtAdapter;
import db.DataProvider;
import helpers.ChatApplication;
import helpers.Endpoints;
import helpers.RESTClient;
import helpers.ResponseParser;
import models.Group;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by minhcao,huyphan on 4/27/16.
 */
public class GroupMgmtFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>{
    private ListView mGroupMgmtListView;
    private PublicGroupMgmtAdapter mAdapter;
    private RESTClient client;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.group_mgmt_fragment, container, false);
        mAdapter = new PublicGroupMgmtAdapter(getActivity(),null,0);
        mGroupMgmtListView = (ListView) view.findViewById(R.id.groupMgmtListView);
        mGroupMgmtListView.setAdapter(mAdapter);
        getLoaderManager().initLoader(0, null, this);
        client =ChatApplication.getInstance().getRestClient();
        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.add_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Dialog dialog = new Dialog(getContext());
                dialog.setContentView(R.layout.group_creation_dialog);
                dialog.setTitle("Group creation");
                final EditText textInput=(EditText) dialog.findViewById(R.id.input_group_name);
                final CheckBox checkbox =(CheckBox)dialog.findViewById(R.id.checkbox_private);
                Button createBtn =(Button)dialog.findViewById(R.id.dialogButtonCreate);
                Button cancelBtn =(Button)dialog.findViewById(R.id.dialogButtonCancel);
                createBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String input = textInput.getText().toString();
                        boolean isPrivate = checkbox.isChecked();
                        if(!input.trim().isEmpty()){
                            if(isPrivate){
                                client.post(Endpoints.GROUP_CREATE_PRIVATE,composeXML(input),new AddGroupCallback(input,"true"));
                            }else{
                                client.post(Endpoints.GROUP_CREATE_PUBLIC,composeXML(input),new AddGroupCallback(input,"false"));
                            }
                            dialog.dismiss();
                        }

                    }
                });
                cancelBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                dialog.show();
            }
        });
        client.get(Endpoints.GROUP_ALL_GET, new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
//
                e.printStackTrace();
                Log.d("ChatApp","mekiep");
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()){
                    response.body().close();
                    throw new IOException("Unexpected code " + response);
                }
                Log.d("ChatApp","mekiep2");
                new ResponseParser().parse(Endpoints.GROUP_ALL_GET, response.body().byteStream());
            }
        });
        return view;
    }

    private String composeXML(String input) {
        return "<group><name>"+input+"</name></group>";
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        String[] projection = {Group.COLUMN_ID, Group.COLUMN_NAME, Group.COLUMN_PRIVACY,Group.COLUMN_SIZE,Group.COLUMN_HASJOINED};
        String selection = Group.COLUMN_PRIVACY +"= '0'";
        CursorLoader cursorLoader = new CursorLoader(getActivity(), DataProvider.GROUP_URI, projection, selection, null, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        mAdapter.swapCursor(data);
    }


    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    private class AddGroupCallback implements Callback{
        private String groupName;
        private String isPrivate;
        public AddGroupCallback(String groupName,String isPrivate){
            this.isPrivate=isPrivate;
            this.groupName=groupName;
        }
        @Override
        public void onFailure(Call call, IOException e) {

        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            ContentValues values = new ContentValues();
            values.put(Group.COLUMN_NAME,groupName);
            values.put(Group.COLUMN_PRIVACY,isPrivate);
            values.put(Group.COLUMN_SIZE,1);
            values.put(Group.COLUMN_HASJOINED,1);
            getContext().getContentResolver().insert(DataProvider.GROUP_URI,values);
        }
    }
}
