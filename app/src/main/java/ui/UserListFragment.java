package ui;

import android.app.LoaderManager;
import android.content.ClipData;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.example.minhcao.chatapp.R;

import adapters.GroupListAdapter;
import adapters.UserListAdapter;
import db.DataProvider;
import helpers.ChatApplication;
import models.Group;
import models.User;


/**
 * Created by minhcao on 4/27/16.
 */
public class UserListFragment extends Fragment implements android.support.v4.app.LoaderManager.LoaderCallbacks<Cursor> {
    private ChatApplication mApp = ChatApplication.getInstance();
    private ListView mUserListView, mGroupListView;
    private UserListAdapter mUserListAdapter;
    private GroupListAdapter mGroupListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.user_list_fragment, container, false);

        mUserListView = (ListView) view.findViewById(R.id.userListView);
        mUserListAdapter = new UserListAdapter(getActivity(), null, 0);
        mUserListView.setAdapter(mUserListAdapter);

        mGroupListView = (ListView) view.findViewById(R.id.groupListView);
        mGroupListAdapter = new GroupListAdapter(getActivity(), null , 0);
        mGroupListView.setAdapter(mGroupListAdapter);

        mUserListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) mUserListAdapter.getItem(position);
                String target = cursor.getString(cursor.getColumnIndexOrThrow(User.COLUMN_USERNAME));
                Intent intent = new Intent(getActivity(), ChatBoxActivity.class);
                intent.putExtra("target", target);
                intent.putExtra("isPrivate", true);
                getActivity().startActivity(intent);
            }
        });

        mGroupListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cursor cursor = (Cursor) mGroupListAdapter.getItem(position);
                String target = cursor.getString(cursor.getColumnIndexOrThrow(Group.COLUMN_NAME));
                Intent intent = new Intent(getActivity(), ChatBoxActivity.class);
                intent.putExtra("target", target);
                intent.putExtra("isPrivate", false);
                getActivity().startActivity(intent);
            }
        });

        // init cursor loader
        getLoaderManager().initLoader(1, null, this);
        getLoaderManager().initLoader(2, null, this);
        return view;
    }

    @Override
    public android.support.v4.content.Loader<Cursor> onCreateLoader(int id, Bundle args) {
        android.support.v4.content.CursorLoader cursorLoader;
        if(id == 1){
            String[] projection = {User.COLUMN_ID, User.COLUMN_FIRSTNAME, User.COLUMN_LASTNAME, User.COLUMN_TITLE, User.COLUMN_USERNAME};
            String selection = User.COLUMN_USERNAME + " != ?";
            String[] selectionArgs = {mApp.getPrefManager().getUsername()};
            cursorLoader = new android.support.v4.content.CursorLoader(getActivity(), DataProvider.USER_URI, projection, selection, selectionArgs, null);
        } else {
            String[] projection = {Group.COLUMN_ID, Group.COLUMN_NAME, Group.COLUMN_PRIVACY, Group.COLUMN_HASJOINED};
            String selection = Group.COLUMN_HASJOINED + " = 1";
            cursorLoader = new android.support.v4.content.CursorLoader(getActivity(), DataProvider.GROUP_URI, projection, selection, null, null);
        }
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(android.support.v4.content.Loader<Cursor> loader, Cursor data) {
        switch(loader.getId()){
            case 1:
                mUserListAdapter.swapCursor(data);
                break;
            case 2:
                mGroupListAdapter.swapCursor(data);
                break;
        }
        ListUtils.setDynamicHeight(mUserListView);
        ListUtils.setDynamicHeight(mGroupListView);
    }

    @Override
    public void onLoaderReset(android.support.v4.content.Loader<Cursor> loader) {
        mUserListAdapter.swapCursor(null);
    }


    public static class ListUtils {
        public static void setDynamicHeight(ListView mListView) {
            BaseAdapter mListAdapter = (BaseAdapter) mListView.getAdapter();
            if (mListAdapter == null) {
                // when adapter is null
                return;
            }
            int height = 0;
            int desiredWidth = View.MeasureSpec.makeMeasureSpec(mListView.getWidth(), View.MeasureSpec.UNSPECIFIED);
            for (int i = 0; i < mListAdapter.getCount(); i++) {
                View listItem = mListAdapter.getView(i, null, mListView);
                listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                height += listItem.getMeasuredHeight();
            }
            ViewGroup.LayoutParams params = mListView.getLayoutParams();
            params.height = height + (mListView.getDividerHeight() * (mListAdapter.getCount() - 1));
            mListView.setLayoutParams(params);
            mListView.requestLayout();
        }
    }
}
