package ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import helpers.ChatApplication;
import helpers.ResponseParser;
import okhttp3.Call;
import okhttp3.Callback;

import com.example.minhcao.chatapp.R;

import java.io.IOException;

import adapters.TabPagerAdapter;
import helpers.Endpoints;
import helpers.RESTClient;
import okhttp3.Response;
import service.PollingService;

public class HomeActivity extends AppCompatActivity {
    private RelativeLayout mLoadingPanel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        Toolbar toolbar = (Toolbar) findViewById(R.id.homeToolbar);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);

        toolbar.setTitle("Hospital ChatApp");
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        String token = ChatApplication.getInstance().getPrefManager().getToken();
        if (token != null) {
            ChatApplication.getInstance().getRestClient().get(Endpoints.USER_GET, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        response.body().close();
                        throw new IOException("Unexpected code " + response);
                    }
                    Log.d("REST", "asd");
                    ResponseParser.parse(Endpoints.USER_GET, response.body().byteStream());
                }
            });

            ChatApplication.getInstance().getRestClient().get(Endpoints.GROUP_JOINED_GET, new Callback() {
                @Override
                public void onFailure(Call call, IOException e) {
                    e.printStackTrace();
                }

                @Override
                public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) {
                        response.body().close();
                        throw new IOException("Unexpected code " + response);
                    }
                    Log.d("REST", "asd");
                    ResponseParser.parse(Endpoints.GROUP_JOINED_GET, response.body().byteStream());
                }
            });
        }

        assert viewPager != null;
        viewPager.setAdapter(new TabPagerAdapter(getSupportFragmentManager(), this));
        assert tabLayout != null;
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent mServiceIntent = new Intent(HomeActivity.this, PollingService.class);
        startService(mServiceIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}