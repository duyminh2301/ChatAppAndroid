package adapters;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhcao.chatapp.R;

import java.util.ArrayList;

import models.Group;

/**
 * Created by minhcao on 4/28/16.
 */
public class GroupListAdapter extends CursorAdapter {
    private Context mContext;

    public GroupListAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        this.mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.group_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = new ViewHolder();

        viewHolder.nameText = (TextView) view.findViewById(R.id.groupName);
        viewHolder.privacyIcon = (ImageView) view.findViewById(R.id.privacyIcon);

        viewHolder.nameText.setText(cursor.getString(cursor.getColumnIndexOrThrow(Group.COLUMN_NAME)));
        boolean isPrivate = cursor.getInt(cursor.getColumnIndexOrThrow(Group.COLUMN_PRIVACY)) == 1;
        if(isPrivate){
            viewHolder.privacyIcon.setImageResource(R.drawable.private_icon);
        } else {
            viewHolder.privacyIcon.setImageResource(R.drawable.public_icon);
        }
    }
    private class ViewHolder {
        TextView nameText;
        ImageView privacyIcon;
    }
}
