package adapters;

import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhcao.chatapp.R;

import java.util.Random;

import models.User;

/**
 * Created by minhcao on 4/27/16.
 */
public class UserListAdapter extends CursorAdapter {
    private Context mContext;
    //    private ArrayList<User> mList = new ArrayList();
    private int[] avatars = {R.drawable.avatar1, R.drawable.avatar2, R.drawable.avatar3};

    public UserListAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        this.mContext = context;
//        mList.add(new User(R.drawable.avatar1, "Minh", "Doctor"));
//        mList.add(new User(R.drawable.avatar2, "Huy", "Guard"));
//        mList.add(new User(R.drawable.avatar3, "Anikka", "Nurse"));
//        mList.add(new User(R.drawable.avatar1, "Minh", "Doctor"));
//        mList.add(new User(R.drawable.avatar2, "Huy", "Guard"));
//        mList.add(new User(R.drawable.avatar3, "Anikka", "Nurse"));
    }

//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        ViewHolder viewHolder;
//
//        if(convertView == null){
//            viewHolder = new ViewHolder();
//            convertView = LayoutInflater.from(mContext).inflate(R.layout.user_item, null);
//            viewHolder.titleText = (TextView) convertView.findViewById(R.id.textTitle);
//            viewHolder.nameText = (TextView) convertView.findViewById(R.id.textName);
//            viewHolder.avatar = (ImageView) convertView.findViewById(R.id.avatarImage);
//            convertView.setTag(viewHolder);
//        } else {
//            viewHolder = (ViewHolder) convertView.getTag();
//        }
//
//
//        User user = mList.get(position);
//        viewHolder.nameText.setText(user.name);
//        viewHolder.titleText.setText(user.title);
//        viewHolder.avatar.setImageBitmap(decodeSampledBitmapFromResource(mContext.getResources(), user.avatarId, 100, 100));
//
//        return convertView;
//    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.user_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = new ViewHolder();
        viewHolder.titleText = (TextView) view.findViewById(R.id.textTitle);
        viewHolder.nameText = (TextView) view.findViewById(R.id.textName);
        viewHolder.avatar = (ImageView) view.findViewById(R.id.avatarImage);

        String name="";
        name = cursor.getString(cursor.getColumnIndexOrThrow(User.COLUMN_FIRSTNAME)) +" "+ cursor.getString(cursor.getColumnIndexOrThrow(User.COLUMN_LASTNAME));
        if(name.equals("null null")){
            name = cursor.getString(cursor.getColumnIndexOrThrow(User.COLUMN_USERNAME));
        }
        viewHolder.nameText.setText(name);
        viewHolder.titleText.setText(cursor.getString(cursor.getColumnIndex(User.COLUMN_TITLE)));
        viewHolder.avatar.setImageBitmap(decodeSampledBitmapFromResource(mContext.getResources(), avatars[new Random().nextInt(3)], 100, 100));
    }

    private static class ViewHolder {
        TextView titleText, nameText;
        ImageView avatar;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }
}
