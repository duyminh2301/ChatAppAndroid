package adapters;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.minhcao.chatapp.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.zip.Inflater;

import helpers.ChatApplication;
import models.Entry;

/**
 * Created by minhcao on 4/28/16.
 */
public class MessageListAdapter extends CursorAdapter {
    private Context mContext;
    private ChatApplication mApplication = ChatApplication.getInstance();
    private String user_username;

    public MessageListAdapter(Context context, Cursor cursor, int flags) {
        super(context, cursor, 0);
        this.mContext = context;
        this.user_username = mApplication.getPrefManager().getUsername();
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        String origin = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_ORIGIN));
        Log.d("message adapter", user_username+ origin);
        if(origin.equals(user_username)){
            return LayoutInflater.from(context).inflate(R.layout.list_item_message_right, parent, false);
        } else {
            return LayoutInflater.from(context).inflate(R.layout.list_item_message_left, parent, false);
        }
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ViewHolder viewHolder = new ViewHolder();

        viewHolder.msgFrom = (TextView) view.findViewById(R.id.lblMsgFrom);
        viewHolder.content = (TextView) view.findViewById(R.id.txtMsg);
        viewHolder.time = (TextView) view.findViewById(R.id.textTime);

        viewHolder.msgFrom.setText(cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_ORIGIN)));
        viewHolder.content.setText(cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_MESSAGE)));

        String createdTime = cursor.getString(cursor.getColumnIndexOrThrow(Entry.COLUMN_TIME));
        Date date = null;
        try {
            date = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(createdTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String formattedDate = new SimpleDateFormat("dd/MM/yyyy, Ka").format(date);

        viewHolder.time.setText(formattedDate);
    }

    private static class ViewHolder {
        TextView msgFrom, content, time;
    }
}
