package adapters;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.minhcao.chatapp.R;

import java.io.IOException;

import db.DataProvider;
import helpers.ChatApplication;
import helpers.Endpoints;
import helpers.RESTClient;
import models.Group;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by beochot on 5/2/2016.
 */
public class PublicGroupMgmtAdapter extends CursorAdapter {
    public PublicGroupMgmtAdapter(Context context, Cursor c, int flags) {
        super(context, c, 0);
        this.mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.group_mgmt_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final ViewHolder viewHolder= new ViewHolder();
        final int rowID=cursor.getInt(cursor.getColumnIndexOrThrow(Group.COLUMN_ID));
        final int size =cursor.getInt(cursor.getColumnIndexOrThrow(Group.COLUMN_SIZE));
        viewHolder.nameText = (TextView) view.findViewById(R.id.publicGroupName);
        viewHolder.sizeText = (TextView) view.findViewById(R.id.groupSize);
        viewHolder.mgmtButton = (Button) view.findViewById(R.id.mgmtButton);
        viewHolder.nameText.setText(cursor.getString(cursor.getColumnIndexOrThrow(Group.COLUMN_NAME)));
        viewHolder.sizeText.setText(String.valueOf(size));
        viewHolder.mgmtButton.setTextColor(ContextCompat.getColor(context, R.color.white));
        if(cursor.getInt(cursor.getColumnIndexOrThrow(Group.COLUMN_HASJOINED))==1){
            viewHolder.mgmtButton.setText("Leave");
            viewHolder.mgmtButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorAccent));
        } else {
            viewHolder.mgmtButton.setText("Join");
            viewHolder.mgmtButton.setBackgroundColor(ContextCompat.getColor(mContext, R.color.colorPrimary));
        }
        final RESTClient client = ChatApplication.getInstance().getRestClient();
        viewHolder.mgmtButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String body = composeXML(viewHolder.nameText.getText().toString());
                if(viewHolder.mgmtButton.getText().equals("Leave")){
                    client.post(Endpoints.GROUP_DO_LEAVE,body,new GroupManageCallback(rowID,0,size-1));
                }else{
                    client.post(Endpoints.GROUP_DO_JOIN,body,new GroupManageCallback(rowID,1,size+1));
                }
            }
        });
    }

    private String composeXML(String text) {
        return "<group><name>"+text+"</name></group>";
    }

    private class ViewHolder {
        TextView nameText, sizeText;
        Button mgmtButton;
    }
    private class GroupManageCallback implements Callback{
        private int rowID;
        private int joinValue;
        private int newSize;
        public GroupManageCallback(int rowID,int value,int size){
            this.rowID=rowID;
            this.joinValue =value;
            this.newSize=size;
        }
        @Override
        public void onFailure(Call call, IOException e) {

        }

        @Override
        public void onResponse(Call call, Response response) throws IOException {
            ContentValues values = new ContentValues();
            values.put(Group.COLUMN_HASJOINED, joinValue);
            values.put(Group.COLUMN_SIZE,newSize);
            String selection =Group.COLUMN_ID+"="+rowID;
            mContext.getContentResolver().update(DataProvider.GROUP_URI,values,selection,null);
        }
    }
}
