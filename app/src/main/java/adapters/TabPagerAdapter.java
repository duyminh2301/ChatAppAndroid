package adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.example.minhcao.chatapp.R;

import ui.GroupMgmtFragment;
import ui.UserListFragment;

/**
 * Created by minhcao on 4/27/16.
 */
public class TabPagerAdapter extends FragmentPagerAdapter {
    private Context mContext;

    public TabPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        mContext = context;
    }
    private int[] imageResId = {
            R.drawable.ic_action_communication_chat,
            R.drawable.ic_action_social_people_outline
    };
    @Override
    public Fragment getItem(int position) {
        switch(position){
            case 0:
                return new UserListFragment();
            case 1:
                return new GroupMgmtFragment();
            default:
                return new UserListFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Drawable image = ContextCompat.getDrawable(mContext, imageResId[position]);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;
    }

}
