package helpers;

import android.util.Log;

import java.util.concurrent.TimeUnit;
import java.io.UnsupportedEncodingException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.ConnectionPool;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

/**
 * Created by minhcao on 4/30/16.
 */
public class RESTClient {
    private OkHttpClient client = new OkHttpClient();

    public void get(String url, Callback callback) {
        Log.d("REST", "in");
        Request request = new Request.Builder()
                .header("Authorization", ChatApplication.getInstance().getPrefManager().getToken())
                .url(url)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public void get_polling(String url, Callback callback){
        ConnectionPool pool = new ConnectionPool(10,5, TimeUnit.MINUTES);
        OkHttpClient clientPolling = new OkHttpClient.Builder().
                connectTimeout(5,TimeUnit.MINUTES).
                readTimeout(5,TimeUnit.MINUTES).
                writeTimeout(5,TimeUnit.MINUTES).
                connectionPool(pool).
                build();
        Request request = new Request.Builder()
                .header("Authorization", ChatApplication.getInstance().getPrefManager().getToken())
                .url(url)
                .build();
        Call call = clientPolling.newCall(request);
        call.enqueue(callback);
    }

    public void post(String url, String body, Callback callback) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/xml; charset=utf-8"), body);
        String token =ChatApplication.getInstance().getPrefManager().getToken();
        Request request = new Request.Builder()
                .header("Authorization", ChatApplication.getInstance().getPrefManager().getToken())
                .url(url)
                .post(requestBody)
                .build();
        Call call = client.newCall(request);
        call.enqueue(callback);
    }

    public void auth(String url, String email, String pwd, Callback callback) {
        Request request = new Request.Builder()
                .url(url)
                .post(new FormBody.Builder()
                        .add("username", email)
                        .add("password", pwd)
                        .build())
                .build();

        Call call = client.newCall(request);
        call.enqueue(callback);
    }
}
