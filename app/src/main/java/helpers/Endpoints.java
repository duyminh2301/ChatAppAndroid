package helpers;

/**
 * Created by minhcao on 4/30/16.
 */
public class Endpoints {
    // localhost for emulator
//    private static final String BASE_APP = "http://10.0.2.2:8080/ChatApp/app/";
    //private static final String BASE_APP = "http://10.0.2.2";  // genymotion
    private static final String BASE_APP = "http://demo-beochot.rhcloud.com/app/";  // local ip

    public static final String USER_GET= BASE_APP + "user";
    public static final String GROUP_JOINED_GET= BASE_APP + "group/";
    public static final String GROUP_ALL_GET= BASE_APP + "group/all";
    public static final String ENTRY_GET = BASE_APP + "history/";
    public static final String GROUP_DO_JOIN= BASE_APP + "group/join";
    public static final String GROUP_DO_LEAVE= BASE_APP + "group/leave";
    public static final String GROUP_CREATE_PUBLIC= BASE_APP + "group/createPublic";
    public static final String GROUP_CREATE_PRIVATE= BASE_APP + "group/createPrivate";
    public static final String MESSAGE_SEND= BASE_APP + "chat/";
    public static final String POLLING= BASE_APP + "chat";

    public static final String LOGIN = BASE_APP + "auth/login";
    public static final String LOGOUT = BASE_APP + "auth/logout";
    public static final String REGISTER = BASE_APP + "auth/register";
}
