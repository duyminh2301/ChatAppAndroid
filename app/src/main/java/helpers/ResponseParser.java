package helpers;

/**
 * Created by minhcao on 4/30/16.
 */

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.util.Log;
import android.util.Xml;
import android.view.View;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import db.DataProvider;
import db.DbHelper;
import models.Entry;
import models.Group;
import models.PrivateEntry;
import models.User;
import ui.ChatBoxActivity;


public class ResponseParser {
    private Activity mActivity;
    private static ChatApplication mApplication = ChatApplication.getInstance();
    private String target;

    public static boolean parse(String url, InputStream stream) {
        switch (url) {
            case Endpoints.GROUP_JOINED_GET:
            case Endpoints.GROUP_ALL_GET:
            case Endpoints.USER_GET:
            case Endpoints.POLLING:
            case Endpoints.ENTRY_GET:
                return parseXML(stream, url);
//            default:
//                if(url.contains(Endpoints.ENTRY_GET)){
//                    return parseXML(stream);
//                }
        }
        return false;
    }

    public static boolean parseXML(InputStream stream, String url) {
        try {
            XmlPullParser parser = Xml.newPullParser();
            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(stream, null);
            parser.nextTag();

            return readXML(parser, url);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
            Log.d("ChatApp", "Xml parser exception");
        } catch (IOException e) {
            Log.d("ChatApp", "SocketTimeoutException because polling timeout on server");

        }
        return false;
    }

    private static boolean readXML(XmlPullParser parser, String url) {
        int event;

        try {
            event = parser.getEventType();

            while (event != XmlPullParser.END_DOCUMENT) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.START_TAG:
                        switch (name) {
                            case "user":
                                readUser(parser);
                                break;
                            case "group":
                                readGroup(parser);
                                break;
                            case "privateMessageEntry":
                                readMessage(parser, true, url);
                                break;
                            case "groupMessageEntry":
                                readMessage(parser, false, url);
                                break;
                        }
                        break;
                }

                event = parser.next();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private static void readUser(XmlPullParser parser) {
        int event;
        String text = null;
        String ownerName = mApplication.getPrefManager().getUsername();
        User u = new User();
        try {
            event = parser.getEventType();

            while ((event != XmlPullParser.END_TAG) || (!parser.getName().equals("user"))) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name.equals("email")) {
                            u.setEmail(text);
                        } else if (name.equals("firstName")) {
                            u.setFirstName(text);
                        } else if (name.equals("lastName")) {
                            u.setLastName(text);
                        } else if (name.equals("privilege")) {
                            u.setRole(text);
                        } else if (name.equals("title")) {
                            u.setTitle(text);
                        } else if (name.equals("username")) {
                            u.setUsername(text);
                        }
                        break;
                }
                event = parser.next();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        // otherwise, add in db
        ContentValues values = new ContentValues();
        values.put(User.COLUMN_EMAIL, u.getEmail());
        values.put(User.COLUMN_TITLE, u.getTitle());
        values.put(User.COLUMN_USERNAME, u.getUsername());
        values.put(User.COLUMN_FIRSTNAME, u.getFirstName());
        values.put(User.COLUMN_LASTNAME, u.getLastName());
        values.put(User.COLUMN_ROLE, u.getRole());
        mApplication.getContentResolver().insert(DataProvider.USER_URI, values);
    }

    private static void readGroup(XmlPullParser parser) {
        int event;
        String text = null;
        Group g = new Group();
        try {
            event = parser.getEventType();

            while ((event != XmlPullParser.END_TAG) || (!parser.getName().equals("group"))) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name.equals("name")) {
                            g.setName(text);
                        } else if (name.equals("isPrivate")) {
                            int privacy;
                            if (text.equals("true")) {
                                privacy = 1;
                            } else {
                                privacy = 0;
                            }
                            g.setPrivate(privacy);
                        } else if (name.equals("size")) {
                            g.setSize(Integer.parseInt(text));
                        } else if (name.equals("hasJoined")) {
                            if (text.equals("true")) {
                                g.setHasJoined(1);
                            }
                        }
                        break;
                }
                event = parser.next();
            }

        } catch (
                Exception e
                )

        {
            e.printStackTrace();
        }

        ContentValues values = new ContentValues();
        values.put(Group.COLUMN_NAME, g.getName());
        values.put(Group.COLUMN_PRIVACY, g.getPrivacy());
        values.put(Group.COLUMN_SIZE, g.getSize());
        if (g.getHasJoined() == 1) {
            values.put(Group.COLUMN_HASJOINED, 1);
        }
        mApplication.getContentResolver().insert(DataProvider.GROUP_URI, values);
    }


    private static void readMessage(XmlPullParser parser, boolean isPrivate, String url) {
        int event;
        String text = null;
        Entry e = new Entry();
        boolean isOrigin = true;
        boolean isIdOfMessage = true; //quick hack to get only id of message because response contains many Id
        try {
            event = parser.getEventType();

            while ((event != XmlPullParser.END_TAG) || (!parser.getName().contains("MessageEntry"))) {
                String name = parser.getName();
                switch (event) {
                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;
                    case XmlPullParser.END_TAG:
                        if (name.equals("username")) {
                            if (isOrigin) {
                                e.setOrigin(text);
                            } else {
                                e.setTarget(text);
                            }
                        } else if (name.equals("origin")) {
                            isOrigin = false;
                        } else if (name.equals("timeCreated")) {
                            e.setTime(text);
                        } else if (name.equals("message")) {
                            e.setMessage(text);
                        } else if (name.equals("name")) {
                            e.setTarget(text);
                        } else if (name.equals("id")) {
                            if (isIdOfMessage) {
                                e.setId(Long.parseLong(text));
                            }
                            isIdOfMessage = false;
                        }
                        break;
                }
                event = parser.next();
            }

        } catch (Exception error) {
            error.printStackTrace();
        }

        ContentValues values = new ContentValues();
        values.put(Entry.COLUMN_MESSAGE, e.getMessage());
        values.put(Entry.COLUMN_ORIGIN, e.getOrigin());
        values.put(Entry.COLUMN_TARGET, e.getTarget());
        values.put(Entry.COLUMN_TIME, e.getTime());
        values.put(Entry.COLUMN_ID, e.getId());
        if (url.equals(Endpoints.POLLING) && !e.getOrigin().equals(mApplication.getPrefManager().getUsername())) {
            Intent resultIntent = new Intent(mApplication, ChatBoxActivity.class);
            resultIntent.putExtra("target", e.getOrigin());
            resultIntent.putExtra("isPrivate", isPrivate);
            new NotificationUtils(mApplication).showNotificationMessage("New Message from " + e.getOrigin(), e.getMessage(), e.getTime(), resultIntent);
        }
        if (isPrivate) {
            mApplication.getContentResolver().insert(DataProvider.PRIVATE_ENTRY_URI, values);
        } else {
            mApplication.getContentResolver().insert(DataProvider.GROUP_ENRTY_URI, values);
        }
    }
}