package helpers;

import android.app.Application;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by minhcao on 4/30/16.
 */
public class ChatApplication extends Application {
    public static final String TAG = ChatApplication.class
            .getSimpleName();

    private static ChatApplication mInstance;

    private MyPreferenceManager pref;
    private RESTClient client;
    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
    }

    public static synchronized ChatApplication getInstance() {
        return mInstance;
    }


    public MyPreferenceManager getPrefManager() {
        if (pref == null) {
            pref = new MyPreferenceManager(this);
        }

        return pref;
    }
    public RESTClient getRestClient(){
        if (client == null) {
            client = new RESTClient();
        }
        return client;
    }
    public boolean isConnected(){
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if(activeNetwork!=null){
            return true;
        }
        return false;
    }
}
