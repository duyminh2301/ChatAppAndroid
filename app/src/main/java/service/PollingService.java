package service;

/**
 * Created by beochot on 5/3/2016.
 */

import android.app.IntentService;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import org.apache.commons.io.ByteOrderMark;
import org.apache.commons.io.input.BOMInputStream;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import helpers.ChatApplication;
import helpers.Endpoints;
import helpers.RESTClient;
import helpers.ResponseParser;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Response;

/**
 * Created by beochot on 3/14/2016.
 */
public class PollingService extends IntentService {
    private AsyncTask<Void,Void,Void> mTask;

    public PollingService(){
        super("PollingService");
    }
    private boolean isRunning=false;
    private RESTClient mClient = ChatApplication.getInstance().getRestClient();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        startPolling();
    }

    private void startPolling(){

        mClient.get_polling(Endpoints.POLLING, new Callback(){
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                startPolling();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (!response.isSuccessful()) {
                    response.body().close();
                    throw new IOException("Unexpected code " + response);
                }
//                Log.d("REST", "is "+ response.body().string());
                ResponseParser.parse(Endpoints.POLLING, response.body().byteStream());
                startPolling();
            }
        });
    }
}
