package receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import helpers.ChatApplication;
import service.PollingService;

/**
 * Created by beochot on 5/3/2016.
 */
public class BootUpReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        if(ChatApplication.getInstance().isConnected()){
            Intent i = new Intent(context, PollingService.class);
            context.startService(i);
        }
    }
}
