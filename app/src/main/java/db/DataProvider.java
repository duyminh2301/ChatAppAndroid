package db;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import models.Group;
import models.GroupEntry;
import models.PrivateEntry;
import models.User;

/**
 * Created by minhcao on 4/30/16.
 */
public class DataProvider extends ContentProvider {

    // database
    private DbHelper helper;
    private SQLiteDatabase database;

    // PATH
    private static final String GROUP_PATH = "group";
    private static final String USER_PATH = "user";
    private static final String PRIVATE_ENTRY_PATH = "privateentry";
    private static final String GROUP_ENRTY_PATH = "groupentry";
    private static final String AUTHORITY = "com.metropolia.app.provider";

    // Content URI
    public static final Uri USER_URI= Uri.parse("content://" + AUTHORITY+ "/" + USER_PATH);
    public static final Uri GROUP_URI= Uri.parse("content://" + AUTHORITY+ "/" + GROUP_PATH);
    public static final Uri PRIVATE_ENTRY_URI= Uri.parse("content://" + AUTHORITY+ "/" + PRIVATE_ENTRY_PATH);
    public static final Uri GROUP_ENRTY_URI= Uri.parse("content://" + AUTHORITY+ "/" + GROUP_ENRTY_PATH);

    // Creates a UriMatcher object.
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);

    // Match value
    private static final int USER = 1;
    private static final int USER_ID = 2;
    private static final int GROUP = 3;
    private static final int GROUP_ID = 4;
    private static final int PRIVATE_ENTRY = 5;
    private static final int GROUP_ENTRY = 6;

    static {
        sUriMatcher.addURI(AUTHORITY, USER_PATH, USER);
        sUriMatcher.addURI(AUTHORITY, USER_PATH + "/#", USER_ID);
        sUriMatcher.addURI(AUTHORITY, GROUP_PATH, GROUP);
        sUriMatcher.addURI(AUTHORITY, GROUP_PATH + "/#", GROUP_ID);
        sUriMatcher.addURI(AUTHORITY, PRIVATE_ENTRY_PATH, PRIVATE_ENTRY);
        sUriMatcher.addURI(AUTHORITY, GROUP_ENRTY_PATH, GROUP_ENTRY);
    }

    @Override
    public boolean onCreate() {
        helper = new DbHelper(getContext());
        database = helper.getWritableDatabase();
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        // SQLiteQueryBuilder is a helper class that creates the
        // proper SQL syntax for us.
        SQLiteQueryBuilder qBuilder = new SQLiteQueryBuilder();
        Log.d("query", "querying"+uri.toString());
        // Set the table we're querying.
        int uriType = sUriMatcher.match(uri);
        switch (uriType) {
            case USER:
                qBuilder.setTables(User.TABLE_NAME);
                Log.d("cp","in user");
                break;
            case USER_ID:
                qBuilder.setTables(User.TABLE_NAME);
                qBuilder.appendWhere(User.COLUMN_ID + "="+ uri.getLastPathSegment());
                break;
            case GROUP:
                qBuilder.setTables(Group.TABLE_NAME);
                break;
            case GROUP_ID:
                qBuilder.setTables(Group.TABLE_NAME);
                qBuilder.appendWhere(Group.COLUMN_ID + "="+ uri.getLastPathSegment());
                break;
            case PRIVATE_ENTRY:
                qBuilder.setTables(PrivateEntry.TABLE_NAME);
                break;
            case GROUP_ENTRY:
                qBuilder.setTables(GroupEntry.TABLE_NAME);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        Log.d("cp", "querying");
        // Make the query.
        Cursor c = qBuilder.query(database,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);
        Log.d("after query", "is "+c.getCount());
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long id = 0;
        String PATH = "";
        int uriType = sUriMatcher.match(uri);
        switch (uriType) {
            case USER:
                id = database.insertWithOnConflict(User.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                PATH=USER_PATH;
                break;
            case GROUP:
                id = database.insertWithOnConflict(Group.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                PATH=GROUP_PATH;
                break;
            case PRIVATE_ENTRY:
                id = database.insertWithOnConflict(PrivateEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                PATH=PRIVATE_ENTRY_PATH;
                break;
            case GROUP_ENTRY:
                id = database.insertWithOnConflict(GroupEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                PATH=GROUP_ENRTY_PATH;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return Uri.parse(PATH + "/" + id);
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int id = 0;
        String PATH = "";
        int uriType = sUriMatcher.match(uri);
        switch (uriType) {
            case USER:
                id = database.update(User.TABLE_NAME,values, selection, selectionArgs);
                PATH=USER_PATH;
                break;
            case GROUP:
                id = database.update(Group.TABLE_NAME,values, selection, selectionArgs);
                PATH=GROUP_PATH;
                break;
            case PRIVATE_ENTRY:
                id = database.update(PrivateEntry.TABLE_NAME, values, selection, selectionArgs);
                PATH=PRIVATE_ENTRY_PATH;
                break;
            case GROUP_ENTRY:
                id = database.update(GroupEntry.TABLE_NAME, values, selection, selectionArgs);
                PATH=GROUP_ENRTY_PATH;
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return id;
    }
}