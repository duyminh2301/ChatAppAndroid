package db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import models.Group;
import models.GroupEntry;
import models.PrivateEntry;
import models.User;

/**
 * Created by minhcao on 4/29/16.
 */
public class DbHelper extends SQLiteOpenHelper{
    private static final String DATABASE_NAME = "chatapp.db";
    private static final int DATABASE_VERSION = 8;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(User.TABLE_CREATE);
        db.execSQL(Group.TABLE_CREATE);
        db.execSQL(GroupEntry.TABLE_CREATE);
        db.execSQL(PrivateEntry.TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop table if existed, all data will be gone!!!
        db.execSQL("DROP TABLE IF EXISTS " + User.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + Group.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + GroupEntry.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + PrivateEntry.TABLE_NAME);
        onCreate(db);
    }
}
